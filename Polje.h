#pragma once
#include<SFML\Graphics.hpp>
#define RADIJ_KROG 20
//Predstavlja eno celico
class Polje
{
public:
	Polje::Polje()
	{
		Randomize();
		Krog.setFillColor(sf::Color::Red);
		Krog.setRadius(RADIJ_KROG);
		Krog.setOrigin(RADIJ_KROG,RADIJ_KROG);
	};
	 //Nastavi Barvo na poljubno barvo
	void Randomize();
	void Draw(sf::RenderWindow* window);
	void PostaviPozicijo(int x,int y);
	sf::CircleShape Krog;
	int getBarva()
	{
		return Barva;
	};
	void setBarva(int inBarva)
	{
		Barva = inBarva;
	}
	//Ponastavi barvo, s katero krog nari�emo na zaslon
	void refresh();
	//Zamenja dve barvi
	void swap(Polje *p)
	{
		int temp = p->getBarva();
		p->setBarva(getBarva());
		this->setBarva(temp);
	};
private:
	enum Barve {rdeca,zelena,modra,vijolicna,rumena, bela};
	int Barva;
	
};