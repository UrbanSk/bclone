#include"Polje.h"
#pragma once

void Polje::PostaviPozicijo(int x, int y)
{
	Krog.setPosition(x,y);
};

//Nastavi barvo na poljubno barvo
void Polje::Randomize()
{
		Barva= (rand() % 6) + 1;
		if(Barva==1) {Krog.setFillColor(sf::Color::Red);}
		if(Barva==2) {Krog.setFillColor(sf::Color::Green);}
		if(Barva==3) {Krog.setFillColor(sf::Color::Blue);}
		if(Barva==4) {Krog.setFillColor(sf::Color::Magenta);}
		if(Barva==5) {Krog.setFillColor(sf::Color::Yellow);}
		if(Barva==6) {Krog.setFillColor(sf::Color::White);}
};
void Polje::refresh()
{
		if(Barva<0){Krog.setFillColor(sf::Color::Black);}
		if(Barva==1) {Krog.setFillColor(sf::Color::Red);}
		if(Barva==2) {Krog.setFillColor(sf::Color::Green);}
		if(Barva==3) {Krog.setFillColor(sf::Color::Blue);}
		if(Barva==4) {Krog.setFillColor(sf::Color::Magenta);}
		if(Barva==5) {Krog.setFillColor(sf::Color::Yellow);}
		if(Barva==6) {Krog.setFillColor(sf::Color::White);}
};

void Polje::Draw(sf::RenderWindow* window)
{
	window->draw(Krog);
};