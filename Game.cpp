﻿#pragma once
#include <SFML\Graphics.hpp>
#include "Game.h"
#include<vector>


void Game::GameLoop() //Game Logic
{
	sf::Event event;
        while (window.pollEvent(event))
        {
			Board1.preveriStanje();
            if (event.type == sf::Event::Closed)
            _GameState=Exiting;
			if (event.type==sf::Event::MouseButtonPressed)
			{
				//Če je pritisnjen levi gumb
				if (event.mouseButton.button == sf::Mouse::Button::Left)
				{
					Board1.manageMouseInput(event.mouseButton.x,event.mouseButton.y);
				}
			}
        }
		
};
void Game::Draw() //Draw Stuff
{
	window.clear();
	Board1.Draw(&window);
	window.display();
};

void Game::Initialize(int height, int width)
{
	if(_GameState!=Uninitialized) return;
	Board Board1;
	window.create(sf::VideoMode(height, width,32),"Game");
	_GameState=Playing;
	//endph
};
void Game::Run()
{
	while(_GameState!=Exiting)
	{
		GameLoop();
		Draw();
	}
	window.close();
};
/*void Game::Initialize(int height, int width)
{
	if(_GameState!=Uninitialized) return;
	window.create(sf::VideoMode(height, width,32),"Game");
	_GameState=Battle;
	//Placeholder
	Image.create(50,50,sf::Color(100,100,0,255));
	Texture.loadFromImage(Image);
	sprite.setTexture(Texture);
	sprite.setPosition(100,100);
	//endph

	while(_GameState!=Exiting)
	{
		GameLoop();
		Draw();
	}
	window.close();
};*/
