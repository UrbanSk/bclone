﻿#pragma once
#include "Board.h"
#include <SFML\Graphics.hpp>
#define BOARD_SIZE_X 600
#define BOARD_SIZE_Y 480
class Game
{
public:
Game::Game()
{
	Game::_GameState = Uninitialized;
	Board1.Initialize(BOARD_SIZE_X,BOARD_SIZE_Y);
};
void Initialize(int height, int width);
void Run();
private:
	void manageMouseInput(int x, int y);
	 sf::Sprite sprite;
	void GameLoop();
	bool isExiting();
	void Draw();
	Board Board1;
sf::RenderWindow window;
enum GameState {Exiting,Playing,Uninitialized};
GameState _GameState;
sf::Image Image;
};