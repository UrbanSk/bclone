﻿#pragma once
#include<vector>
#include<SFML\Graphics\RectangleShape.hpp>
#include<SFML\Graphics.hpp>
#include"Polje.h"
#define BOARD_SIZE 64*8
#define SQUARE_DIMENSION  64
#define X_OFFSET 100
#define Y_OFFSET 100
#define SQUARE_THICKNESS 5
#define SELECT_THICKNESS 3
#define ST_STOLPCEV 8
#define ST_VRSTIC 8
#define ZACETEK_POZ_KROG 32

class Board
{
public:
	Board::Board()
	{
	};
	Board::Board(int sizeX,int sizeY)
	{
		//sf::RectangleShape GridRect(X_OFFSET,Y_OFFSET,SQUARE_DIMENSION,SQUARE_DIMENSION);
		GridRect.setPosition(X_OFFSET,Y_OFFSET);
		GridRect.setSize(sf::Vector2f(SQUARE_DIMENSION,SQUARE_DIMENSION));
		GridRect.setOutlineColor(sf::Color::White);
		GridRect.setOutlineThickness(SQUARE_THICKNESS);
		dimensionX=sizeX;
		dimensionY=sizeY;
		stPolj=dimensionX*dimensionY;
		RandomizeFields();
	};
	void Initialize(int sizeX, int sizeY)
	{
		SelectRect.setOutlineThickness(10);
		SelectRect.setFillColor(sf::Color::Transparent);
		SelectRect.setOutlineColor(sf::Color::Red);
		SelectRect.setSize(sf::Vector2f(SQUARE_DIMENSION,SQUARE_DIMENSION));
		GridRect.setPosition(X_OFFSET,Y_OFFSET);
		GridRect.setSize(sf::Vector2f(SQUARE_DIMENSION,SQUARE_DIMENSION));
		GridRect.setOutlineColor(sf::Color::White);
		GridRect.setFillColor(sf::Color::Black);
		GridRect.setOutlineThickness(SQUARE_THICKNESS);
		dimensionX=sizeX;
		dimensionY=sizeY;
		stPolj=dimensionX*dimensionY;
		RandomizeFields();
		int i1,i2;
		for(int i1=0;i1<ST_VRSTIC;i1++)
		{
			for(i2=0;i2<ST_STOLPCEV;i2++)
			{
				Polja[i1][i2].PostaviPozicijo(ZACETEK_POZ_KROG+i2*SQUARE_DIMENSION,ZACETEK_POZ_KROG+i1*SQUARE_DIMENSION);
			}
		}

		preveriStanje();
	};
	void preveriStanje()
	{
		while(preveriSosede())
		{
			cascade();
		}
		for(int x1=0;x1<ST_VRSTIC;x1++)
		{
			if(Polja[0][x1].getBarva()==-1)
			{
				Polja[0][x1].Randomize();
			}
		}
		for(int i1=0;i1<ST_VRSTIC;i1++)
		{
			for(int i2=0;i2<ST_STOLPCEV;i2++)
			{
			Polja[i1][i2].refresh();
			}
		}
	};
	void Draw(sf::RenderWindow* window);
	int dimensionX,dimensionY;
	sf::RectangleShape GridRect;
	sf::RectangleShape SelectRect;

//Ko igralec klikne na celico, jo izberemo
void izberiCelico(int , int );
void prekliciIzbrano();
//Če je ena celica že izbrana vemo, da moramo ravnati drugače
bool izbrana;
int izbranaX;
int izbranaY;
//Kliči samo, če je izbrana true in igralec klikne na sosedno celico
void zamenjaj(int, int);
void manageMouseInput(int x, int y);
private:
	void cascade();
	bool preveriSosede();
	int stPolj;
	Polje Polja[ST_VRSTIC][ST_STOLPCEV];
	void RandomizeFields();
	void DrawBoard(sf::RenderWindow* window);
	void DrawRect(int x, int y,sf::RenderWindow* window);
	sf::RenderWindow window;
};