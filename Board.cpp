﻿#pragma once
#include <Windows.h>
#include "Board.h"
#include "Polje.h"
#include <vector>
#include <SFML\Graphics.hpp>

void Board::Draw(sf::RenderWindow* window)
{
	DrawBoard(window);
		int i1,i2;
		for(i1=0;i1<ST_VRSTIC;i1++)
		{
			for(i2=0;i2<ST_STOLPCEV;i2++)
			{
				Polja[i1][i2].Draw(window);
			}
		}
	window->draw(SelectRect);
};

void Board::RandomizeFields()
{
	int i1,i2;
		for(i1=0;i1<ST_VRSTIC;i1++)
		{
			for(i2=0;i2<ST_STOLPCEV;i2++)
			{
				Polja[i1][i2].Randomize();
			}
		}
};


void Board::cascade()
{
	boolean premaknil = true;
	
	//Kaskado ponavljamo, dokler se ničesar več ne spremeni
	while(premaknil)
	{

		premaknil=false;
		//Preveri od spodaj navzdol, zadnje vrstice ni treba, ce pod poljem prazno(-1)prekopiraj v spodnjo
		for(int y=ST_VRSTIC-2;y>=0;y--)
		{
			for(int x=ST_STOLPCEV-1;x>=0;x--)
			{
				//Če je polje pod trenutnim poljem prazno
				if(Polja[y+1][x].getBarva()<0)
				{
					premaknil=true;
					Polja[y+1][x].setBarva(Polja[y][x].getBarva());
					if(y!=0)//Če to ni najbolj zgornja vrstica tu sedaj nastane luknja
					{
						Polja[y][x].setBarva(-1);
					}
					else //drugače potrebujemo povsem novo barvo
					{
						Polja[y][x].Randomize();
					}
				}
			}
		}
	}
};

bool Board::preveriSosede()
{
	bool cleared=false;
	for (int y=0;y<ST_VRSTIC;y++)
	{
		for (int x=0;x<ST_STOLPCEV;x++)
		{
			//Če dano polje ni kot
			if(!(((x==0)||(x==ST_VRSTIC-1))&&((y==0)||y==ST_STOLPCEV-1)))
			{
				//za prvo in zadnjo vrstico preverjamo samo vodoravno
				if((y==0)||(y==ST_VRSTIC-1))
				{
					//Če so barve treh po vrsti vodoravno enake
					if((abs(Polja[y][x-1].getBarva())==abs(Polja[y][x].getBarva()))&&(abs(Polja[y][x].getBarva())==abs(Polja[y][x+1].getBarva())))
					{
						cleared=true;
						char S[100];
						if(Polja[y][x-1].getBarva()>0)Polja[y][x-1].setBarva(-Polja[y][x-1].getBarva());
						if(Polja[y][x].getBarva()>0)Polja[y][x].setBarva(-Polja[y][x].getBarva());
						if(Polja[y][x+1].getBarva()>0)Polja[y][x+1].setBarva(-Polja[y][x+1].getBarva());
						sprintf(S,"Vodoravno se ujemajo, sredinski:[%d][%d]",y,x);
						OutputDebugStringA(S);
					}
				}
				//za prvi in zadnji stolpev preverjamo samo navpično
				else if((x==0)||(x==ST_STOLPCEV-1))
				{
					if((abs(Polja[y-1][x].getBarva())==abs(Polja[y][x].getBarva()))&&(abs(Polja[y][x].getBarva())==abs(Polja[y+1][x].getBarva())))
					{
						cleared=true;
						char S[100];
						if(Polja[y][x].getBarva()>0)Polja[y][x].setBarva(-Polja[y][x].getBarva());
						if(Polja[y-1][x].getBarva()>0)Polja[y-1][x].setBarva(-Polja[y-1][x].getBarva());
						if(Polja[y+1][x].getBarva()>0)Polja[y+1][x].setBarva(-Polja[y+1][x].getBarva());
						sprintf(S,"Navpicno se ujemajo, sredinski:[%d][%d]\n",y,x);
						OutputDebugStringA(S);
					}
				}
				//drugace preverimo oboje
				else
				{
					if((abs(Polja[y][x-1].getBarva())==abs(Polja[y][x].getBarva()))&&(abs(Polja[y][x].getBarva())==abs(Polja[y][x+1].getBarva())))
					{
						cleared=true;
						if(Polja[y][x-1].getBarva()>0)Polja[y][x-1].setBarva(-Polja[y][x-1].getBarva());
						if(Polja[y][x].getBarva()>0)Polja[y][x].setBarva(-Polja[y][x].getBarva());
						if(Polja[y][x+1].getBarva()>0)Polja[y][x+1].setBarva(-Polja[y][x+1].getBarva());
						char S[100];
						sprintf(S,"Vodoravno se ujemajo, sredinski:[%d][%d]\n",y,x);
						OutputDebugStringA(S);
					}
					if((abs(Polja[y-1][x].getBarva())==abs(Polja[y][x].getBarva()))&&(abs(Polja[y][x].getBarva())==abs(Polja[y+1][x].getBarva())))
					{
						cleared=true;
						if(Polja[y][x].getBarva()>0)Polja[y][x].setBarva(-Polja[y][x].getBarva());
						if(Polja[y-1][x].getBarva()>0)Polja[y-1][x].setBarva(-Polja[y-1][x].getBarva());
						if(Polja[y+1][x].getBarva()>0)Polja[y+1][x].setBarva(-Polja[y+1][x].getBarva());
						char S[100];
						sprintf(S,"Navpicno se ujemajo, sredinski:[%d][%d]\n",y,x);
						OutputDebugStringA(S);
					}
				}
			}
		}
	}
	return cleared;

};

void Board::DrawBoard(sf::RenderWindow* window)
{
	int i1,i2;
	for(i1=5;i1<SQUARE_DIMENSION*ST_STOLPCEV;i1+=SQUARE_DIMENSION)//ŠT STOLPCEV
		{
			for(i2=5;i2<SQUARE_DIMENSION*ST_VRSTIC;i2+=SQUARE_DIMENSION)//ŠT VRSTIC
			{
			 DrawRect(i1,i2,window);
			}
		}

};

void Board::DrawRect(int x, int y,sf::RenderWindow* window)
{
	Board::GridRect.setPosition(x,y);
	window->draw(GridRect);
};

void Board::izberiCelico(int x, int y)
{
	SelectRect.setOutlineColor(sf::Color::Red);
	SelectRect.setPosition(x*SQUARE_DIMENSION,y*SQUARE_DIMENSION);
	izbrana=true;
	izbranaX=x;
	izbranaY=y;
};

void Board::prekliciIzbrano()
{
	SelectRect.setOutlineColor(sf::Color::Black);
	SelectRect.setPosition(-100,-100);
	izbrana=false;
};

void Board::zamenjaj(int x, int y)
{
	Polja[y][x].swap(&Polja[izbranaY][izbranaX]);
	prekliciIzbrano();
};

void Board::manageMouseInput(int x, int y)
{
	int iX=x/SQUARE_DIMENSION;
	int iY=y/SQUARE_DIMENSION;
	//Če igralec klikne iz polja, njegov izbor zanemarimo
	if(x>BOARD_SIZE||x<0||y>BOARD_SIZE||y<0)
		return;
	//Če nimamo izbrane celice, moramo izbrati eno
	if(!izbrana)
	{
		izberiCelico(iX,iY);
	}
	//tu tri možnosti -
	//če klikne na isto celico preklice izbor
	//če klikne na oddaljeno celico izbere to celico
	//če klikne na bližnjo celico jo izbere
	else
	{
		//oddaljena
		if(abs(izbranaX-iX)>1||
			abs(izbranaY-iY)>1||
			(abs(izbranaY-iY)==1&&
						abs(izbranaX-iX)))
		{
			izberiCelico(iX,iY);
		}
		//Ista
		else if(iX==izbranaX&&iY==izbranaY)
		{
			prekliciIzbrano();
		}
		//Bliznja
		else
		{
			zamenjaj(iX,iY);
		}
		
	}
	preveriStanje();
};